import sqlite3 as lite
import sys

con = lite.connect('IOT.db')

with con:

    cur = con.cursor()
    cur.execute("CREATE TABLE STATUS(Stat_Id INT, TimeStamp TEXT, Sensor_1 INT, Sensor_2 INT, Sensor_3 INT, Sensor_4 INT, Pump1 INT, Pump2 INT, Pump3 INT, Pump4 INT)")
    cur.execute("CREATE TABLE SETTINGS(Frequency INT, Duration INT, Sensor_1_LOW INT, Sensor_2_LOW INT, Sensor_3_LOW INT, Sensor_4_LOW INT, Sensor_1_HIGH INT, Sensor_2_HIGH INT, Sensor_3_HIGH INT, Sensor_4_HIGH INT, Pump1 INT, Pump2 INT, Pump3 INT, Pump4 INT)")
