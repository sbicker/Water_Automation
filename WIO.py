from flask import Flask
from flask import Markup
from flask import Flask
from flask import render_template
from flask import request
import pygal
import sqlite3 as lite
import sys

app = Flask(__name__)

@app.route('/')
def chart():
    try:
        con = lite.connect('IOT.db')
        settings_con = lite.connect('IOT.db')
        settings_cur = settings_con.cursor()
        cur = con.cursor()
        settings_cur.execute("SELECT * FROM SETTINGS")
        cur.execute("SELECT * FROM STATUS ORDER BY Stat_Id DESC LIMIT 10")
        rows = cur.fetchall()
        settings_rows = settings_cur.fetchall()
        labels = []
        S1 = []
        S2 = []
        S3 = []
        S4 = []
        settings_cur
        s_stat1=0
        s_stat2=0
        s_stat3=0
        s_stat4=0
        for r in rows:
            labels.append(r[1])
            S1.append(r[2])
            S2.append(r[3])
            S3.append(r[4])
            S4.append(r[5])
            s_stat1=int(r[2])
            s_stat2=int(r[3])
            s_stat3=int(r[4])
            s_stat4=int(r[5])
        sf=0
        sd=0
        st1=0
        st2=0
        st3=0
        st4=0
        for sr in settings_rows:
            sf=sr[0]
            sd=sr[1]
            st1=sr[2]
            st2=sr[3]
            st3=sr[4]
            st4=sr[5]
        graph = pygal.Line()
        graph.title = 'Sensor Values over time.'
        graph.x_labels = labels
        graph.add('Sensor 1', S1)
        graph.add('Sensor 2', S2)
        graph.add('Sensor 3', S3)
        graph.add('Sensor 4', S4)
        graph_data = graph.render_data_uri()
        stat_g = pygal.SolidGauge(inner_radius=0.70)
        percent_formatter = lambda x: '{:.10g}%'.format(x)
        stat_g.value_formatter = percent_formatter
        stat_g.add(
            'Sensor 1', [
                {'value': s_stat1, 'max_value': 100},
                {'value': 20, 'max_value': 100}])
        stat_g.add(
            'Sensor 2', [
                {'value': s_stat2, 'max_value': 100},
                {'value': 20, 'max_value': 100}])
        stat_g.add(
            'Sensor 3', [
                {'value': s_stat3, 'max_value': 100},
                {'value': 20, 'max_value': 100}])
        stat_g.add(
            'Sensor 4', [
                {'value': s_stat4, 'max_value': 100},
                {'value': 20, 'max_value': 100}])
        stat_data = stat_g.render_data_uri()
        return render_template("index.htm", graph_data = graph_data, stat_data=stat_data, sf=sf, sd=sd, st1=st1, st2=st2, st3=st3, st4=st4)
    except Exception as e:
        return(str(e))


@app.route('/Update_Settings', methods=['POST'])
def Update_Settings():
    sf=request.form.get("txt_cf")
    sd=request.form.get("txt_pd")
    st1=request.form.get("txt_s1")
    st2=request.form.get("txt_s2")
    st3=request.form.get("txt_s3")
    st4=request.form.get("txt_s4")
    conu = lite.connect('IOT.db')
    curu = conu.cursor()
    curu.execute("DROP TABLE SETTINGS")
    curu.execute("CREATE TABLE SETTINGS(Frequency INT, Duration INT, Sensor_1_LOW INT, Sensor_2_LOW INT, Sensor_3_LOW INT, Sensor_4_LOW INT, Sensor_1_HIGH INT, Sensor_2_HIGH INT, Sensor_3_HIGH INT, Sensor_4_HIGH INT, Pump1 INT, Pump2 INT, Pump3 INT, Pump4 INT)")
    sql_command="INSERT INTO SETTINGS (Frequency, Duration, Sensor_1_LOW, Sensor_2_LOW, Sensor_3_LOW, Sensor_4_LOW) VALUES (" + str(sf) + ", " + str(sd) + ", " + str(st1) + ", " + str(st2) + ", " + str(st3) + ", " + str(st4) + ");"
    print(sql_command)
    curu.execute(sql_command)
    conu.commit()
    return render_template("save.htm")

@app.errorhandler(Exception)
def exception_handler(error):
    return "!!!!"  + repr(error)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)
